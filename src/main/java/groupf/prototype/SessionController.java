package prototype;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.validation.BindingResult;            
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.RequestMethod;   
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;
import java.util.logging.Logger;

//Controller for handling Session details
@Controller
public class SessionController {
    private static final String formPath = "/form";
    private static final Logger logger = Logger.getLogger(SessionController.class.getName());


    //Displays Session Form
     @RequestMapping(value=formPath, method=RequestMethod.GET)
    public String showSessionForm(SessionForm sessionForm) {
        logger.info("Fetching display page for form");
        return "form";
    }


    //Controller fpr dealing with adding sessions
    @RequestMapping(value = formPath, method = RequestMethod.POST)           
    public String addNewSession(SessionForm sessionForm,  BindingResult result, 
    RedirectAttributes redirectAttributes) {                                                            
        logger.info("Submitting form");
        boolean needsRedirected = false; //Redirect the page back to the form if errors found
                                                                           
        
        Integer sessionDay, sessionMonth, sessionYear,
            sessionTime, sessionDuration;


        sessionTime = Integer.valueOf(sessionForm.getTime());
        sessionDuration = Integer.valueOf(sessionForm.getDuration());

        if(sessionForm.getDate() == null) {
            logger.warning("Invalid Date entered");
            redirectAttributes.addFlashAttribute("dateError", "Please enter a valid date");
            needsRedirected = true;
        }
        else {
            sessionDay = sessionForm.getDate().getDate();
            sessionMonth = sessionForm.getDate().getMonth();
            sessionYear  = sessionForm.getDate().getYear();

            //Not Current Year
            if(sessionYear != 2013) {
                logger.warning("Session for the year "+sessionYear+" entered instead of current year");
                redirectAttributes.addFlashAttribute("dateError", "Only enter a session for the current year");
                needsRedirected = true;
            }
            //Session on christmas day
            else if(sessionMonth == 11 && sessionDay == 25) {
                logger.warning("Session on christmas entered");
                redirectAttributes.addFlashAttribute("dateError", "Cannot have a session on Christmas");
                needsRedirected = true;
            }
        }


        //No Time
        if(sessionForm.getTime() == "") {
            logger.warning("No time entered");
            redirectAttributes.addFlashAttribute("timeError", "No starting time entered");
            needsRedirected = true;
        }
        //Out of hours
        else if(sessionTime > 17 || sessionTime < 8) {
            logger.warning("Out of hours time added");
            redirectAttributes.addFlashAttribute("timeError", "Session must start between 8am and 5pm inclusive");
            needsRedirected = true;
        }
        //0 Attendance
        if(sessionForm.getMaxAttendance() == 0) {
            logger.warning("Invalid attendance entered: "+sessionForm.getMaxAttendance());
            redirectAttributes.addFlashAttribute("attendanceError", "Must be a whole number greater than 0");
            needsRedirected = true;
        }

        //No duration
        if(sessionForm.getDuration() == "") {
            logger.warning("No duration entered");
            redirectAttributes.addFlashAttribute("durationError", "No duration entered");
            needsRedirected = true;
        }
        //Less than an hour or greater than 3 hours
        else if(sessionDuration < 1 || sessionDuration > 3) {
            logger.warning("Not valid duration: "+sessionDuration);
            redirectAttributes.addFlashAttribute("durationError", "Must be between 1 and 3 hours inclusive");
            needsRedirected = true;
        }
        else if(sessionDuration + sessionTime > 18) {
            logger.warning("Session ends too late at "+(sessionDuration+sessionTime)+":00");
            redirectAttributes.addFlashAttribute("durationError", "Session must end before 18:00");
            needsRedirected =true;
        }

        //No frequency
        if(sessionForm.getFrequency() == "") {
            logger.warning("No frequency entered");
            redirectAttributes.addFlashAttribute("frequencyError", "No frequency entered");
            needsRedirected = true;
        }

        if(needsRedirected) {
            logger.info("Redirecting back to page with errors displayed instead of submitting");
            return "redirect:"+formPath; 
        }

        logger.info("SessionForm instance created with properties \n"+sessionForm);
        logger.info("Redirecting to success page");


        return "result"; //Send to result/sucess page
    }


}
