
package prototype;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewSessionController {

    @RequestMapping("/sessionToday")
    public String todayRequest() {
       
        return "sessionToday";
    }


    @RequestMapping("/sessionWeek")
    public String weekRequest() {

        return "sessionWeek";
    }



    @RequestMapping("/sessionForever")
    public String foreverRequest() {

        return "sessionForever";
    }




}
