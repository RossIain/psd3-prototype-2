package prototype;

import sun.util.calendar.LocalGregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Calendar;
import java.util.logging.Logger;
import java.util.Date;
import java.util.GregorianCalendar;
public class SessionForm {

    private String lecturer;
    private int maxAttendance;
    private String compulsoryVenue;
    private String duration;
    private String frequency;
    private String day;
    private String month;
    private String year;
    private String time;

        public String getLecturer() {
			return lecturer;
		}
		public void setLecturer(String lecturer) {
			this.lecturer = lecturer;
		}

		public int getMaxAttendance() {
			return maxAttendance;
		}
		public void setMaxAttendance(int maxAttendance) {
			this.maxAttendance = maxAttendance;
		}
		public String getCompulsoryVenue() {
			return compulsoryVenue;
		}
		public void setCompulsoryVenue(String compulsoryVenue) {
			this.compulsoryVenue = compulsoryVenue;
		}
		public String getDuration() {
			return duration;
		}
		public void setDuration(String duration) {
			this.duration = duration;
		}
		public String getFrequency() {
			return frequency;
		}
		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}

        //Don't care Date's depreciated, Gregorian Calendar is giving 1 for the year
        //2 for the month and 5 for the day no matter what I do, so until Sun
        //actually decide to make an actual working straightforward simple Date class
        //I'll use this >.>
        //TODO fix so not using Depreciated Date class
		public Date getDate() {
            int y =Integer.valueOf(year);
            int m =Integer.valueOf(month);
            int d =Integer.valueOf(day);
            if(!validDate(y,m,d))
                return null;
            Date date = new Date(y,m,d);
            return date;

		}

        public String getDay() {
            return day;
        }

        public String getMonth() {
            return month;
        }

        public String getYear() {
            return year;
        }

        public void setDay(String d ){day=d;}
        public void setMonth(String m){month=m;}
        public void setYear(String y){year=y;}



		public String getTime() {
			return time;
		}
		public void setTime(String time) {
			this.time = time;
		}


		public String toString() {
			String s = "";
			s+=("Lecturer: "+getLecturer()+"\n");
			s+=("Max Attendance: "+getMaxAttendance()+"\n");
			s+=("Compulsory Venue: "+getCompulsoryVenue()+"\n");
			s+=("Duration: "+getDuration()+"\n");
			s+=("Frequency: " +getFrequency()+"\n");
			s+=("Date: "+getDate().getDay()+"/"+(getDate().getMonth()+1)+"/"+getDate().getYear()+"\n");
			s+=("Time: "+getTime()+"\n");

			return s;

		}


        private boolean validDate(int year, int month, int day) {
            if(day > 31 || day < 1)
                return false;
            if(day > 30 && month == 1 || month == 3 || month == 5 || month == 8 || month == 10)
                return false;
            if(day > 29 && month == 1)
                return false;
            if(day >28 && month ==1 && (year%4!=0 || year%100 == 0 && year%400 != 0))
                return false;
            return true;
        }
}
