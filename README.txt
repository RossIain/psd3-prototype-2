==== TEAM F Prototype 2 Walkthrough ====


== Requirements: == 
This prototype requires JRE 7

Running this from the Hoved Server will NOT work, as it only has JRE/JDk 6 not 7
and causes a null pointer exception when starting the server.

It is recomended this prototype be run on a x64 machine with a linux distribution installed
as this has only been tested on linux machines, but should work on any System with JRE 7. 


== Starting Program ==

If on a linux machine the dispatch servlet can be invoked by running "./StartDispatch.sh"
while in the same directory as the README otherwise enter 
   "java -jar build/libs/gs-serving-web-content-0.1.0.jar --server.port=8081"

You should see the spring boot logo, and after a few seconds a "Started Application" log message
in the console/terminal.

Ensure you don't have any other services using port 8081 otherwise
either close them or run 
    "java -jar build/libs/gs-serving-web-content-0.1.0.jar --server.port=[yourport]"

from the same directory as this file.

Linux Only: If this doesn't work a build might be required, enter ./gradlew build
and wait for the build to complete and try again. 


== Entering Sessions ==
Start a web browser, and enter http://localhost:8081/form (or if you entered a 
self specified port replace 8081 with that) to bring up
a Session form, and enter in the details for a Session.

Note: there are certain non-functional requirements assumed, you
can either investigate these yourself by trying to input a session
and reading the error messages if you don't meet one or more of the
requirements, or read the nonfunctional.txt in the
same directory as this file.

Once all the appropriate details have been entered, then press the
"submit" button, if all the appropriate details have been entered then
you will be brought to a "success page" and asked if you want to enter
another session, press the button to enter another session if you wish.

If you view the terminal in which you started the dispatch servlet
you should be able to see log messages showing the session data which
is now captured by the program. Also the terminal shows any erros
and general logging information associated with your actions
while submitting details into the form.

Otherwise if any details are missing or don't meet the non-functional
requirements, then a message will come up next to each field
stateing what is wrong.

== Viewing Sessions ==

If you now enter into the address bar in your browser
http://localhost:8081/sessionToday  (or if you entered a 
self specified port replace 8081 with that) 

and it should bring up the current Session for a day,
resizing the window should resize the table.

Click on the buttons at the top right to view the details
for a session week or session details forever.

